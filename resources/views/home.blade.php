@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    const app = new Vue({
        el: '#app',
        mounted(){
            this.listen();
            this.whisper();
        },
        methods: {
            listen(){
                console.log('listen....');
                /*
                Echo.private('Task.5b4b28e5fe57c600284320ab')
                    .listen('ProcessOutput', function(data) {
                        console.log(JSON.stringify(data));
                    });
                    */
                this.channel = Echo.private('chat')
                    .listenForWhisper('typing', (e) => {
                        console.log(e.message);
                    });
            },
            whisper(){
                setInterval(() => {
                    this.channel.whisper('typing', { message: 'Howdy!' });
                    console.log('chatting');
                }, 2000);
                
            }
        }
    });
</script>
@endsection