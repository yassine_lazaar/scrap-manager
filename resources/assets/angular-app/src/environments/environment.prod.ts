const packageJson = require('../../package.json');

export const environment = {
  production: true,
  envName:'PROD',
  serverUrl: 'http://api.scrapmanager.test/api/',
  versions: {
    app: packageJson.version,
    angular: packageJson.dependencies['@angular/core'],
    ngrx: packageJson.dependencies['@ngrx/store'],
    material: packageJson.dependencies['@angular/material'],
    rxjs: packageJson.dependencies.rxjs,
    typescript: packageJson.devDependencies['typescript']
  } 
};
