import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { TestingComponent } from '@app/testing/testing.component';

const routes: Routes = [
  { path: 'testing', component: TestingComponent, pathMatch: 'full' },
  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { /*enableTracing: true*/ })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
