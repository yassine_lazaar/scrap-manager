import { ModuleWithProviders, NgModule } from '@angular/core';
import { ECHO_CONFIG, EchoConfig, EchoService } from './echo.service';

@NgModule({
  imports: [],
  providers: [
    EchoService
  ]
})
export class EchoModule {
  public static forRoot(config: EchoConfig): ModuleWithProviders {
    return {
      ngModule: EchoModule,
      providers: [
        EchoService,
        { provide: ECHO_CONFIG, useValue: config },
      ]
    }
  }
}