import { EchoModule } from './echo.module';

describe('EchoModule', () => {
  let angularLaravelEchoModule: EchoModule;

  beforeEach(() => {
    angularLaravelEchoModule = new EchoModule();
  });

  it('should create an instance', () => {
    expect(angularLaravelEchoModule).toBeTruthy();
  });
});
