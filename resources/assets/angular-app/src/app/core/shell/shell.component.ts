import { Component, OnInit, HostBinding, ViewEncapsulation } from '@angular/core'
import { selectorAuth, AuthLogoutAction } from '@app/core/auth/auth.reducer'
import { Store } from '@ngrx/store'
import { MatSnackBar } from '@angular/material/snack-bar'
import { ErrorHandlerInterceptor } from '@app/core/http/error-handler.interceptor'
import { routeAnimations } from '@app/core/animations/route.animations'

import { environment as env } from '@env/environment';
import { NotificationService, Notification } from '@app/core/notifications/notifications.service';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [routeAnimations]
})
export class ShellComponent implements OnInit {

  isAuthenticated: boolean = false

  constructor(
    private _store: Store<any>,
    private _errorHandler: ErrorHandlerInterceptor,
    private _notificationService: NotificationService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this._store
      .select(selectorAuth)
      .subscribe(auth => (this.isAuthenticated = auth.isAuthenticated));

    this._errorHandler.errors.subscribe((message: string) => this.snackBar.open(message, 'OK'))
    this._notificationService.notifications.subscribe(
      (notification: Notification) => this.snackBar.open(notification.message, notification.dismissAction)
    )
  }

  logout() {
    this._store.dispatch(new AuthLogoutAction())
  }

}
