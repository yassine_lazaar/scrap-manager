import { Injectable } from '@angular/core'
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http'
import { Observable, Subject } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { Logger } from '@app/core/logger.service';

/**
 * Adds a default error handler to all requests.
 */
@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

  private errors$: Subject<string>;

  get errors(): Observable<string> {
    return this.errors$.asObservable();
  }

  constructor(private _log:Logger) {
    this.errors$ = new Subject<string>();
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(error => this.errorHandler(error)));
  }


  // Customize the default error handler here if needed
  private errorHandler(response: HttpEvent<any>): Observable<HttpEvent<any>> {
    if (response instanceof HttpErrorResponse) {
      // Server or connection error happened
      if (!navigator.onLine) {
        // Handle offline error
        this.errors$.next('Please check your internet connection')
      } else {
        // Handle Http Error (error.status === 403, 404...)
        if (response.status === 500) {
          this.addError(response.message)
        } else {
          console.log(response)
          let _message = response.error.errors.message
          let message = ((_message instanceof Object) ? _message[Object.keys(_message)[0]] : _message)
          this.addError(message)
        }
      }
    }
    // TODO: Handle Client Error (Angular Error, ReferenceError...)    
    this._log.error('Throwing Error')
    throw response
  }

  addError(message: string) {
    this.errors$.next(message);
  }

}
