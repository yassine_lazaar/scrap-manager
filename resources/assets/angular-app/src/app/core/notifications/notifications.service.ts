import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

export class Notification {
  constructor(
    public message,
    public dismissAction: string = 'OK'
  ){}
}

@Injectable()
export class NotificationService {

  private _notification$: Subject<Notification>


  get notifications(): Observable<Notification> {
    return this._notification$.asObservable();
  }
  
  constructor() {
    this._notification$ = new Subject<Notification>();
  }

  addNotification(notification: Notification){
    this._notification$.next(notification)
  }

}