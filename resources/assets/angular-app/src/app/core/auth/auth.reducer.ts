import { Action } from '@ngrx/store'
import { User } from './user'

export const AUTH_KEY = 'AUTH'
export const selectorAuth = state => state.auth

export enum AuthActionTypes {
  LOGIN = '[Auth] Login',
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGIN_ERROR = '[Auth] Login Error',
  LOGOUT = '[Auth] Logout',
  LOGOUT_SUCCESS = '[Auth] Logout Success',
  LOGOUT_ERROR = '[Auth] Logout Error'
}

export class AuthLoginAction implements Action {
  readonly type = AuthActionTypes.LOGIN
  constructor(public payload: User) { }
}

export class AuthLoginSuccessAction implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS
  constructor(public payload: any) { }
}

export class AuthLoginErrorAction implements Action {
  readonly type = AuthActionTypes.LOGIN_ERROR
  constructor(public payload: any) { }
}

export class AuthLogoutAction implements Action {
  readonly type = AuthActionTypes.LOGOUT
}

export class AuthLogoutSuccessAction implements Action {
  readonly type = AuthActionTypes.LOGOUT_SUCCESS
  constructor(public payload: any) { }
}

export class AuthLogoutErrorAction implements Action {
  readonly type = AuthActionTypes.LOGOUT_ERROR
  constructor(public payload: any) { }
}

export type AuthActions =
  AuthLoginAction |
  AuthLoginSuccessAction |
  AuthLoginErrorAction |
  AuthLogoutAction |
  AuthLogoutSuccessAction |
  AuthLogoutErrorAction

export interface AuthState {
  isAuthenticated: boolean
  accessToken?: string,
  expiresIn?: number,
  tokenType?: string,
  user?: string
}

export const initialState: AuthState = {
  isAuthenticated: false
}

export function authReducer(state: AuthState = initialState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.LOGIN:
      return { ...state }
    case AuthActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        accessToken: <any>action.payload.accessToken,
        expiresIn: <any>action.payload.expiresIn,
        tokenType: <any>action.payload.tokenType,
        user: <any>action.payload.user
      }
    case AuthActionTypes.LOGIN_ERROR:
      return {
        ...state,
        isAuthenticated: state.isAuthenticated,
        accessToken: state.accessToken,
        expiresIn: state.expiresIn,
        tokenType: state.tokenType,
        user: state.user
      }
    case AuthActionTypes.LOGOUT:
      return { ...state }
    case AuthActionTypes.LOGOUT_SUCCESS:
      return {
        ...state,
        isAuthenticated: false,
        accessToken: null,
        expiresIn: null,
        tokenType: null,
        user: null
      }
    case AuthActionTypes.LOGOUT_ERROR:
      return {
        ...state,
        isAuthenticated: state.isAuthenticated,
        accessToken: state.accessToken,
        expiresIn: state.expiresIn,
        tokenType: state.tokenType,
        user: state.user
      }
    default:
      return state
  }
}