import { Injectable } from '@angular/core'
import { User } from './user'
import { Observable } from 'rxjs'
import { HttpClient, HttpResponse } from '@angular/common/http'


@Injectable()
export class AuthService {


  constructor(
    private _http: HttpClient
  ) { }

  login(user: User): Observable<HttpResponse<any>> {
    const loginUrl: string = 'auth/login'
    return this._http.post<HttpResponse<any>>(loginUrl, user)
  }

  logout(): Observable<HttpResponse<any>> {
    const logoutUrl: string = 'auth/logout'
    return this._http.post<any>(logoutUrl, null)
  }
}
