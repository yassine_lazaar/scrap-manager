import { Injectable } from '@angular/core'
import { Actions, Effect } from '@ngrx/effects'
import { AuthService } from './auth.service'
import { LocalStorageService } from '../local-storage/local-storage.service'
import { Router } from '@angular/router'
import {
  AUTH_KEY,
  AuthActionTypes,
  AuthActions,
  AuthLoginAction,
  AuthLoginSuccessAction,
  AuthLoginErrorAction,
  AuthLogoutSuccessAction,
  AuthLogoutErrorAction
} from './auth.reducer'
import { catchError, map, switchMap, tap } from 'rxjs/operators'
import { Observable, of } from 'rxjs'

export interface Token {
  accessToken: string,
  tokenType: string,
  expiresIn: number,
  user: string
}

@Injectable()
export class AuthEffects {

  constructor(
    private _actions: Actions<AuthActions>,
    private _localStorage: LocalStorageService,
    private _authService: AuthService,
    private _router: Router,
  ) { }

  @Effect()
  login(): Observable<AuthActions> {
    return this._actions
      .ofType(AuthActionTypes.LOGIN).pipe(
        switchMap(action => {
          const payload = (action as AuthLoginAction).payload
          if (payload.email !== '' && payload.password !== '') {
            return this._authService.login(payload).pipe(
              map((response:any) => new AuthLoginSuccessAction(response.data)),
              catchError(error => of(new AuthLoginErrorAction(error)))
            )
          }
        })
      )
  }

  @Effect({ dispatch: false })
  loginSuccess(): Observable<AuthActions> {
    return this._actions
      .ofType(AuthActionTypes.LOGIN_SUCCESS).pipe(
        tap((action: AuthLoginSuccessAction) => {
          const token = action.payload
          this._localStorage.setItem(AUTH_KEY, { 
            isAuthenticated: true,
            accessToken: token.accessToken,
            expiresIn: token.expiresIn,
            tokenType: token.tokenType,
            user: token.user
          })
          this._router.navigate(['/tasks'])
        })
      )
  }

  @Effect()
  logout(): Observable<AuthActions> {
    return this._actions
      .ofType(AuthActionTypes.LOGOUT).pipe(
        switchMap(action => {
          return this._authService.logout().pipe(
            map(response => new AuthLogoutSuccessAction(response)),
            catchError(error => of(new AuthLogoutErrorAction(error)))
          )
        })
      )
  }

  @Effect({ dispatch: false })
  logoutSuccess(): Observable<AuthActions> {
    return this._actions
      .ofType(AuthActionTypes.LOGOUT_SUCCESS).pipe(
        tap((response: any) => {
          this._localStorage.setItem(AUTH_KEY, { isAuthenticated: false })
          this._router.navigate(['/'])
        })
      )
  }

}