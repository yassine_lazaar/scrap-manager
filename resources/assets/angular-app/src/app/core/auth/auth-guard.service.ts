import { Injectable } from '@angular/core'
import { CanActivate } from '@angular/router'
import { Store } from '@ngrx/store'
import { selectorAuth } from './auth.reducer'

@Injectable()
export class AuthGuard implements CanActivate {

  _isAuthenticated: boolean = false

  constructor(private _store: Store<any>) {
    this._store
      .select(selectorAuth)
      .subscribe(auth => this._isAuthenticated = auth.isAuthenticated)
  }

  canActivate(): boolean {
    return !this._isAuthenticated
  }

}