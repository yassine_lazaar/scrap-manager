import { NgModule, Optional, SkipSelf } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { MetaReducer, StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { RouterModule } from '@angular/router'
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { OverlayContainer } from '@angular/cdk/overlay'

import { initStateFromLocalStorage } from './meta-reducers/init-state-from-local-storage.reducer'
import { debug } from './meta-reducers/debug.reducer'
import { authReducer } from './auth/auth.reducer'
import { AuthEffects } from './auth/auth.effects'
import { LocalStorageService } from './local-storage/local-storage.service'
import { AuthService } from './auth/auth.service'
import { AuthGuard } from './auth/auth-guard.service'
import { ApiPrefixInterceptor } from './http/api-prefix.interceptor'
import { CacheInterceptor } from './http/cache.interceptor'
import { ErrorHandlerInterceptor } from './http/error-handler.interceptor'
import { HttpCacheService } from './http/http-cache.service'
import { HttpService } from './http/http.service'
import { ShellComponent } from './shell/shell.component'
import { Logger } from './logger.service'
import { AnimationsService } from './animations/animations.service'
import { NotificationService } from './notifications/notifications.service'


import { SharedModule } from '@app/shared'
import { JwtModule } from '@auth0/angular-jwt'

export function tokenGetter() {
  return LocalStorageService.getToken()
}

export const metaReducers: MetaReducer<any>[] = [debug, initStateFromLocalStorage]

@NgModule({
  imports: [
    // Angular 
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,

    // Shared
    SharedModule,

    // Ngrx
    StoreModule.forRoot(
      { auth: authReducer },
      {
        metaReducers
      }),
    EffectsModule.forRoot([AuthEffects]),

    // jwt
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['api.scrapmanager.test'],
        blacklistedRoutes: ['http://api.scrapmanager.test/api/auth/login']
      }
    }),
  ],
  declarations: [ShellComponent],
  providers: [
    LocalStorageService,
    Logger,
    AuthService,
    AuthGuard,
    ApiPrefixInterceptor,
    ErrorHandlerInterceptor,
    CacheInterceptor,
    HttpCacheService,
    {
      provide: HttpClient,
      useClass: HttpService
    },
    AnimationsService,
    NotificationService
  ]
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule,
    overlayContainer: OverlayContainer
  ) {
    overlayContainer.getContainerElement().classList.add('unicorn-dark-theme');
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}