import { Component, OnInit } from '@angular/core';
import { Subject } from "rxjs";
import { Observable, interval } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { EchoService } from '../echo/echo.service';
import { map } from "rxjs/operators";
import { log } from 'util';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {

  private tasks: any;
  private privateChannel: EchoService;

  constructor(private http: HttpClient, private echoService: EchoService) {
    //this.privateChannel = this.echoService.join('chat', 'private');
  }

  ngOnInit() {
    const channelName = 'App.User.' + localStorage.getItem('user');
    /*
    this.echoService.join(channelName, 'private')
      .listen(channelName, 'ProcessOutput')
      .pipe(
        map((event: any) => console.log(event))
      ).subscribe();
    */

    /*
    this.privateChannel.listenForWhisper('chat', 'typing')
      .subscribe((event) => {
        console.log(event)
      }, (error) => {
        console.log('Something happened ' + error)
      })
    */

    /*
    this.echoService.join('tasks', 'public')
      .listen('tasks', 'ProcessOutput')
      .pipe(
        map((event: any) => console.log(event))
      ).subscribe();
    */

    /*
    console.log('ngOnInit');
    const scanUrl = 'http://api.scrapmanager.test/api/scan';
    this.http.get(scanUrl).subscribe((response:any) => {
      this.tasks = JSON.parse(response.data);
      console.log(this.tasks);
    });
    */

    /*
    const tick$ = interval(1000);
    const subject = new Subject();
    subject.subscribe((x => console.log("Observer 1 " + x)));
    subject.subscribe((x => console.log("Observer 2 " + x)));
    tick$.subscribe(subject);
    */

    this.echoService.join('task', 'public')
      .listen('task', '.task.output')
      .pipe(
        map((event: any) => console.log(event))
      ).subscribe();

  }


  emitSocket() {
    /*
    this.privateChannel.whisper('chat', 'typing', {
      message: 'This is for the backend.'
    })
    */
  }

}
