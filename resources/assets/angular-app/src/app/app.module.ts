import { NgModule } from '@angular/core'
import { Router } from '@angular/router'

import { AppComponent } from './app.component'
import { AppRoutingModule } from './app-routing.module'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { CoreModule } from '@app/core'
import { SharedModule } from '@app/shared'
import { LoginModule } from '@app/login/login.module'
import { TasksModule } from '@app/tasks/tasks.module'
import { TestingComponent } from '@app/testing/testing.component';

@NgModule({
  imports: [
    // Angular

    // Core and shared
    CoreModule,
    SharedModule,

    // App
    LoginModule,
    TasksModule,
    AppRoutingModule,

    // Store debug
    StoreDevtoolsModule.instrument({
      maxAge: 25
    })
  ],
  declarations: [AppComponent, TestingComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  // Diagnostic only: inspect router configuration
  constructor(router: Router) {
    // console.log('Routes: ', JSON.stringify(router.config, undefined, 2))
  }

}
