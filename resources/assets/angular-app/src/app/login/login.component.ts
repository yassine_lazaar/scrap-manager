import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Store } from '@ngrx/store'
import { AuthLoginAction, selectorAuth } from '@app/core'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup
  formSubmitAttempt: boolean

  constructor(
    private _formBuilder: FormBuilder,
    private _store: Store<any>
  ) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      email: ['', Validators.required && Validators.email],
      password: ['', Validators.required]
    });

    this._store.select(selectorAuth)
  }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

  onSubmit() {
    if (this.form.valid)
      this._store.dispatch(new AuthLoginAction(this.form.value))
  }

}
