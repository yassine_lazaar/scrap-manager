import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule } from '@angular/forms'

import { LoginRoutingModule } from './login-routing.module'
import { LoginComponent } from '@app/login/login.component'
import { CoreModule } from '@app/core'
import { SharedModule } from '@app/shared'

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,

    CoreModule,
    SharedModule,
    LoginRoutingModule,
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
