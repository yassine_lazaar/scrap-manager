import { Injectable } from '@angular/core'
import { Effect, Actions } from '@ngrx/effects'
import { TerminalActions, TerminalActionsTypes, TerminalOutputReceivedAction } from './terminal.reducer'
import { Observable } from 'rxjs'
import { TaskService } from './task.service'
import { map, switchMap } from 'rxjs/operators'

@Injectable()
export class TerminalEffects {

  constructor(
    private _actions: Actions,
    private _taskService: TaskService
  ) { }

  @Effect()
  getOutput(): Observable<TerminalActions> {
    return this._actions
      .ofType(TerminalActionsTypes.GET_OUTPUT)
      .pipe(
        switchMap((action: any) => {
          return this._taskService.getOutput(action.payload)
            .pipe(
              map(response => new TerminalOutputReceivedAction(action.payload))
            )
        })
      )
  }

}