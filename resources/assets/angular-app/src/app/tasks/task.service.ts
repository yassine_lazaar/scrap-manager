import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http'
import { map, tap, catchError } from 'rxjs/operators'
import { ScannedTask } from './scanned-task'
import { Task } from '@app/tasks/task';

@Injectable()
export class TaskService {

  private _scanUrl: string = 'scan'
  private _loadTasksUrl: string = 'task'
  private _startTaskUrl: string = 'task/start'
  private _outputTaskUrl: string = 'task/output'
  _taskOutput$: Observable<any>

  constructor(
    private _http: HttpClient
  ) { }

  public getScans(): Observable<HttpResponse<any>> {
    return this._http.get<HttpResponse<any>>(this._scanUrl)
      .pipe(
        map((response: any) => response.data)
      )
  }

  public getTasks(): Observable<HttpResponse<any>> {
    return this._http.get<HttpResponse<any>>(this._loadTasksUrl)
      .pipe(
        map((response: any) => response.data)
      )
  }

  public startTask(task: ScannedTask) {
    return this._http.post<HttpResponse<any>>(this._startTaskUrl, task)
  }

  public getOutput(task: Task): Observable<HttpResponse<any>> {
    return this._http.post<HttpResponse<any>>(this._outputTaskUrl, task)
  }
}
