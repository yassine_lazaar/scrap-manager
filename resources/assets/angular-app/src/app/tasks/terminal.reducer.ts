import { Action } from '@ngrx/store'
import { Task } from './task'

export const selectorTerminal = state => state.terminal

export enum TerminalActionsTypes {
  GET_OUTPUT = '[Terminal] Get Output',
  RECEIVED_OUTPUT = '[Terminal] Received Output',
}

export class TerminalGetOutputAction implements Action {
  readonly type = TerminalActionsTypes.GET_OUTPUT
  constructor(public payload: Task) { }
}

export class TerminalOutputReceivedAction implements Action {
  readonly type = TerminalActionsTypes.RECEIVED_OUTPUT
  constructor(public payload: Task) { }
}

export type TerminalActions =
  TerminalGetOutputAction |
  TerminalOutputReceivedAction

export interface terminalState {
  viewedTaskId: string
}

export const initialTerminalState: terminalState = {
  viewedTaskId: ''
}

export const terminalReducer = (state: terminalState = initialTerminalState, action: Action) => {
  switch (action.type) {
    case TerminalActionsTypes.GET_OUTPUT:
      return { ...state, viewedTaskId: (action as TerminalGetOutputAction).payload._id }
    case TerminalActionsTypes.RECEIVED_OUTPUT:
      return state
    default:
      return state
  }
}