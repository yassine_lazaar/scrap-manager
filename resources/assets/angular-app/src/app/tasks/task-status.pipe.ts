import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'taskStatus' })
export class TaskStatusPipe implements PipeTransform {
  transform(value: number): string {
    switch (value) {
      case value = 0:
        return 'PENDING'
      case value = 1:
        return 'STARTED'
      case value = 2:
        return 'FINISHED WITH SUCCESS'
      case value = 3:
        return 'FINISHED WITH ERROR'
      default:
        return 'PENDING'
    }
  }
}