import { Action } from '@ngrx/store'
import { ScannedTask } from './scanned-task'

export enum ScanActionTypes {
  SCAN = '[Scan]',
  SCAN_SUCCESS = '[Scan] Success',
  SCAN_ERROR = '[Scan] Error'
}

export const selectorScan = state => state.scans

export class ScanAction implements Action {
  readonly type = ScanActionTypes.SCAN
}

export class ScanSuccessAction implements Action {
  readonly type = ScanActionTypes.SCAN_SUCCESS
  constructor(public payload: any) { }
}

export class ScanErrorAction implements Action {
  readonly type = ScanActionTypes.SCAN_ERROR
  constructor(public payload: any) { }
}

export type ScanActions =
  ScanAction |
  ScanSuccessAction |
  ScanErrorAction

export interface ScanState {
  items: ScannedTask[],
}

export const initialScanState: ScanState = {
  items: []
}

export const scanReducer = (state = initialScanState, action: ScanActions) => {
  switch (action.type) {
    case ScanActionTypes.SCAN:
      return { ...state }
    case ScanActionTypes.SCAN_SUCCESS:
      return { ...state, items: (action as ScanSuccessAction).payload }
    case ScanActionTypes.SCAN_ERROR:
      return { ...state, items: [] }
    default:
      return state
  }
}

