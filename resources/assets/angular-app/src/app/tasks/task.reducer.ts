import { Action } from '@ngrx/store'

import { ScannedTask } from './scanned-task'
import { Task } from './task'

export const selectorTask = state => state.tasks

export enum TaskActionTypes {
  LOAD = '[Task] Load',
  LOAD_SUCCESS = '[Task] Load Success',
  LOAD_ERROR = '[Task] Load Error',
  UPDATE = '[Task] Update',
  FILTER = '[Task] Filter',
  START = '[Task] Start',
  START_SUCCESS = '[Task] Start Success',
  START_ERROR = '[Task] Start Error',
  VIEW_OUTPUT = '[Task] View Output',
}

export type TaskActions =
  TaskLoadAction |
  TaskLoadSuccessAction |
  TaskLoadErrorAction |
  TaskUpdateAction |
  TaskStartAction |
  TaskStartSuccessAction |
  TaskStartErrorAction


export class TaskLoadAction implements Action {
  readonly type = TaskActionTypes.LOAD
}

export class TaskLoadSuccessAction implements Action {
  readonly type = TaskActionTypes.LOAD_SUCCESS
  constructor(public payload: Task[]) { }
}

export class TaskLoadErrorAction implements Action {
  readonly type = TaskActionTypes.LOAD_ERROR
  constructor(public payload: any) { }
}

export class TaskUpdateAction implements Action {
  readonly type = TaskActionTypes.UPDATE
  constructor(public payload: Task) { }
}

export class TaskFilterAction implements Action {
  readonly type = TaskActionTypes.FILTER
  constructor(public payload: TaskFilter) { }
}

export class TaskStartAction implements Action {
  readonly type = TaskActionTypes.START
  constructor(public payload: ScannedTask) { }
}

export class TaskStartSuccessAction implements Action {
  readonly type = TaskActionTypes.START_SUCCESS
  constructor(public payload: any) { }
}

export class TaskStartErrorAction implements Action {
  readonly type = TaskActionTypes.START_ERROR
  constructor(public payload: any) { }
}

export interface TaskState {
  items: Task[],
  filter: TaskFilter
}

export const initialTaskState: TaskState = {
  items: [],
  filter: 'ALL'
}

export type TaskFilter = 'ALL' | 'DONE' | 'RUNNING';

export const taskReducer = (state = initialTaskState, action: Action) => {
  switch (action.type) {
    case TaskActionTypes.LOAD:
      return state
    case TaskActionTypes.LOAD_SUCCESS:
      return { ...state, items: (action as any).payload }
    case TaskActionTypes.LOAD_ERROR:
      return state
    case TaskActionTypes.UPDATE:
      return {
        ...state, items: state.items.map(
          item => (action as any).payload.task._id === item._id ? (action as any).payload.task : item
        )
      }
    case TaskActionTypes.FILTER:
      return { ...state, filter: (action as any).payload }
    case TaskActionTypes.START:
      return state
    case TaskActionTypes.START_SUCCESS:
      return { ...state, items: [(action as any).payload.data].concat(state.items) }
    case TaskActionTypes.START_ERROR:
      return state
    default:
      return state
  }
}
