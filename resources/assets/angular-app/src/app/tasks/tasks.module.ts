import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import { TasksRoutingModule } from './tasks-routing.module'
import { TasksListComponent } from './tasks-list/tasks-list.component'
import { CoreModule } from '@app/core'
import { SharedModule } from '@app/shared'
import { TaskService } from '@app/tasks/task.service'
import { scanReducer } from '@app/tasks/scan.reducer'
import { ScanEffects } from '@app/tasks/scan.effects'
import { taskReducer } from '@app/tasks/task.reducer'
import { TaskEffects } from '@app/tasks/task.effects'
import { terminalReducer } from '@app/tasks/terminal.reducer'
import { SocketIoEchoConfig } from '../echo/echo.service'
import { PusherEchoConfig } from '../echo/echo.service'
import { EchoModule } from '@app/echo/echo.module'
import { TerminalEffects } from '@app/tasks/terminal.effects';
import { FormatService } from '@app/tasks/format.service';
import { TaskStatusPipe } from '@app/tasks/task-status.pipe';


/* (<any>window).Pusher = require('pusher-js');
(<any>window).Pusher.logToConsole = true;
export const echoConfig: PusherEchoConfig = {
  userModel: 'App.Models.User',
  notificationNamespace: 'App\\Events',
  options: {
    broadcaster: 'pusher',
    key: '24b8fb4de82d628a480a',
    cluster: 'eu',
    encrypted: true
  }
} */


export const echoConfig: SocketIoEchoConfig = {
  userModel: 'App.Models.User',
  notificationNamespace: 'App\\Events',
  options: {
    broadcaster: 'socket.io',
    host: 'localhost:6001'
  }
}


@NgModule({
  imports: [
    CommonModule,
    TasksRoutingModule,
    CoreModule,
    SharedModule,

    // Echo
    EchoModule.forRoot(echoConfig),

    // ngrx
    StoreModule.forFeature('tasks', taskReducer),
    StoreModule.forFeature('scans', scanReducer),
    StoreModule.forFeature('terminal', terminalReducer),
    EffectsModule.forFeature([
      ScanEffects, 
      TaskEffects,
      TerminalEffects
    ])
  ],
  declarations: [
    TasksListComponent,
    TaskStatusPipe
  ],
  providers: [
    TaskService,
    FormatService
  ]
})
export class TasksModule { }
