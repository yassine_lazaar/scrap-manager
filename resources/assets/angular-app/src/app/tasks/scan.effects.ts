import { Injectable } from '@angular/core'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { switchMap, catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import {
  ScanActions,
  ScanActionTypes,
  ScanSuccessAction,
  ScanErrorAction
} from '@app/tasks/scan.reducer';
import { TaskService } from '@app/tasks/task.service';
import { LocalStorageService } from '@app/core';

export const SCANS_KEY = 'SCANS'

@Injectable()
export class ScanEffects {

  constructor(
    private _actions: Actions,
    private _taskService: TaskService,
    private _localStorage: LocalStorageService
  ) { }

  @Effect()
  scan(): Observable<ScanActions> {
    return this._actions
      .ofType(ScanActionTypes.SCAN)
      .pipe(
        switchMap(() => {
          return this._taskService.getScans().pipe(
            map((response: any) => new ScanSuccessAction(response)),
            catchError(error => of(new ScanErrorAction(error)))
          )
        })
      )
  }

  @Effect({ dispatch: false })
  scanSuccess(): Observable<ScanActions> {
    return this._actions
      .ofType(ScanActionTypes.SCAN_SUCCESS)
      .pipe(
        tap((action: any) => {
          this._localStorage.setItem(SCANS_KEY, { items: action.payload })
        })
      )
  }

  @Effect({ dispatch: false })
  scanError(): Observable<ScanActions> {
    return this._actions
      .ofType(ScanActionTypes.SCAN_ERROR)
      .pipe(
        tap((error: any) => {
          this._localStorage.removeItem(SCANS_KEY)
        })
      )
  }


}