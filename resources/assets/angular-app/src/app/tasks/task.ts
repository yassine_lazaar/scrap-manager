
export interface Task {
  _id: string,
  crawler: string,
  directory: string,
  startedBy: string,
  startTime: number,
  status: number,
}