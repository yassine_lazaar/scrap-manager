import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { Route, AuthGuard} from '@app/core';


const routes: Routes = [
  Route.withShell([
    { path: 'tasks', component: TasksListComponent, data: { title: 'Tasks' } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
