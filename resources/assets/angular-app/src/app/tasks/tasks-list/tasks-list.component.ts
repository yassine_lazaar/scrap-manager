import { Component, OnInit } from '@angular/core'
import { Subject, Observable } from 'rxjs'
import { Store } from '@ngrx/store'
import { Terminal } from 'xterm';
import { fit, proposeGeometry } from 'xterm/lib/addons/fit/fit'

import { selectorScan, ScanAction } from '@app/tasks/scan.reducer'
import { selectorTask, TaskStartAction, TaskLoadAction, TaskUpdateAction, TaskState, TaskFilterAction, TaskFilter } from '@app/tasks/task.reducer'
import { ScannedTask } from '@app/tasks/scanned-task'
import { selectorTerminal, TerminalGetOutputAction } from '@app/tasks/terminal.reducer'
import { Task } from '@app/tasks/task'
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core/animations/route.animations'
import { environment as env } from '@env/environment'
import { EchoService } from '@app/echo/echo.service'
import { tap } from 'rxjs/operators'
import { map } from 'rxjs/operators'
import { selectorAuth } from '@app/core';
import { NotificationService, Notification } from '@app/core/notifications/notifications.service';

const terminalOptions = {
  theme: {
  },
}

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss'],
  /* host: {
    'class': 'router-flex'
  } */
})
export class TasksListComponent implements OnInit {

  scans$: Subject<ScannedTask[]> = new Subject<ScannedTask[]>()
  tasks$: Subject<Task[]> = new Subject<Task[]>()
  terminal$: Subject<any> = new Subject<any>()
  private _xterm: Terminal
  private _authUserId
  taskState: TaskState

  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  versions = env.versions;

  constructor(
    private _store: Store<any>,
    private _echoService: EchoService,
    private _notificationService: NotificationService
  ) {
    this._xterm = new Terminal(terminalOptions)

    _store.select(selectorAuth).subscribe(state => {
      this._authUserId = state.user
    })

    _store.select(selectorScan).subscribe(state => {
      this.scans$.next(state.items)
    })
    _store.select(selectorTask).subscribe(state => {
      this.taskState = state
      this.tasks$.next(this.filterdTasks)
    })
  }

  ngOnInit() {
    this._store.dispatch(new ScanAction())
    this._store.dispatch(new TaskLoadAction())

    this._xterm.open(document.getElementById('terminal'))
    fit(this._xterm)
    this.terminal$.subscribe((event) => {
      this._xterm.write(event.buffer + '/\r')
    })

    this.subscribeToUpdates();
  }

  private subscribeToUpdates() {
    const channel = 'Tasks.' + this._authUserId
    this._echoService.join(channel, 'private')
      .listen(channel, '.task.update')
      .pipe(
        map((task: any) => {
          this._store.dispatch(new TaskUpdateAction(task))
        })
      ).subscribe()
  }

  get filterdTasks(): Task[] {
    const filter = this.taskState.filter
    switch (filter) {
      case 'DONE':
        return this.taskState.items.filter(item => item.status > 0)
      case 'RUNNING':
        return this.taskState.items.filter(item => item.status == 1)
      default:
        return this.taskState.items
    }
  }

  filterTasks(filter: TaskFilter) {
    return this._store.dispatch(new TaskFilterAction(filter))
  }

  startTask(task: ScannedTask) {
    //this._echoService.logout()
    this._xterm.clear()
    this._store.dispatch(new TaskStartAction(task))
    console.log(task)
    this.terminalSubscribe()
    this._notificationService.addNotification(new Notification('Starting ' + task.crawler + ' crawler'))
  }

  viewOutput(task: Task) {
    this._xterm.clear()
    this._store.dispatch(new TerminalGetOutputAction(task))
    this.terminalSubscribe()
  }

  private terminalSubscribe() {
    this._store.select(selectorTerminal).subscribe(state => {
      if (state.viewedTaskId) {
        const channel = 'Task.' + state.viewedTaskId
        this._echoService.join(channel, 'private')
          .listen(channel, '.process.output')
          .pipe(
            tap((event: any) => {
              this.terminal$.next(event)
            })
          ).subscribe()
      }
    })
  }

}