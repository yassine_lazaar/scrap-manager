import { Effect, Actions } from '@ngrx/effects'
import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { switchMap, map, catchError, tap, mergeMap } from 'rxjs/operators'
import { TaskActions, TaskActionTypes, TaskStartSuccessAction, TaskStartErrorAction, TaskStartAction, TaskLoadAction, TaskLoadSuccessAction, TaskLoadErrorAction, TaskUpdateAction } from './task.reducer'
import { TaskService } from '@app/tasks/task.service';
import { TerminalGetOutputAction, TerminalActions } from './terminal.reducer'
import { LocalStorageService, Logger } from '@app/core';

export const TASKS_KEY = 'TASKS'

@Injectable()
export class TaskEffects {

  constructor(
    private _actions: Actions,
    private _taskService: TaskService,
    private _localStorageService: LocalStorageService,
    private _log: Logger
  ) { }

  @Effect()
  load(): Observable<TaskActions> {
    return this._actions
      .ofType(TaskActionTypes.LOAD)
      .pipe(
        switchMap((action: TaskLoadAction) => {
          return this._taskService.getTasks()
            .pipe(
              map((response: any) => new TaskLoadSuccessAction(response)),
              catchError(error => of(new TaskLoadErrorAction(error)))
            )
        })
      )
  }

  @Effect({ dispatch: false })
  loadSuccess(): Observable<TaskActions> {
    return this._actions
      .ofType(TaskActionTypes.LOAD_SUCCESS)
      .pipe(
        tap((action: any) => {
          this._localStorageService.setItem(TASKS_KEY, { items: action.payload , filter: 'ALL'})
        })
      )
  }

  @Effect({ dispatch: false })
  loadError(): Observable<TaskActions> {
    return this._actions
      .ofType(TaskActionTypes.LOAD_ERROR)
      .pipe(
        tap((action: any) => this._log.error(action.payload))
      )
  }

  @Effect()
  start(): Observable<TaskActions> {
    return this._actions
      .ofType(TaskActionTypes.START)
      .pipe(
        switchMap((action: TaskStartAction) => {
          return this._taskService.startTask(action.payload)
            .pipe(
              map(response => new TaskStartSuccessAction(response)),
              catchError(error => of(new TaskStartErrorAction(error)))
            )
        })
      )
  }

  @Effect()
  startSuccess(): Observable<TerminalActions> {
    return this._actions
      .ofType(TaskActionTypes.START_SUCCESS)
      .pipe(
        switchMap((action) => {
          const task = (action as TaskStartSuccessAction).payload.data
          return of(new TerminalGetOutputAction(task))
        })
      )
  }

  @Effect({ dispatch: false })
  startError(): Observable<TaskActions> {
    return this._actions
      .ofType(TaskActionTypes.START_ERROR)
      .pipe(
        tap((action: any) => this._log.error(action.payload))
      )
  }
}