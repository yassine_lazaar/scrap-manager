<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Foundation\Http\Events\RequestHandled;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\TaskCreated' => [
            'App\Listeners\TaskCreatedListener',
        ],
        /*
        'App\Events\ProcessOutput' => [
            'App\Listeners\ProcessOutputListener',
        ],*/
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        /*
        Event::listen(RequestHandled::class, function (RequestHandled $event) {
            //
        });
        */
    }
}
