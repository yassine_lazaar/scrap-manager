<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\TaskLauncher\TaskLauncher as TaskLauncher;

class TaskLauncherServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('TaskLauncher', function ($app) {
            return new TaskLauncher();
        });
    }
}
