<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Response as Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        response()->macro('success', function ($data) {
            return response()->json([
                'errors' => false,
                'data' => $data,
            ]);
        });

        response()->macro('error', function ($message, $status = Response::HTTP_UNPROCESSABLE_ENTITY, $additional_info = []) {
            return response()->json([
                'message' => $status . ' error',
                'errors' => [
                    'message' => $message,
                    'info' => $additional_info,
                ],
                'status_code' => $status,
            ], $status);
        });

        \Horizon::auth(function ($request) {
            return true;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerDevPackages();
    }

    /**
     * Register dev packages.
     *
     * @return void
     */
    private function registerDevPackages()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
