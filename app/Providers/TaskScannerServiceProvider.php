<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\TaskScanner\TaskScanner as TaskScanner;

class TaskScannerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('TaskScanner', function ($app) {
            return new TaskScanner('scraps');
        });
    }
}
