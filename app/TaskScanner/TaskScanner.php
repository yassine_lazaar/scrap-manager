<?php

namespace App\TaskScanner;

use Storage;
use Symfony\Component\Process\Process;

class TaskScanner
{
    private $baseDirectoryName;

    public function __construct(string $baseDirectoryName)
    {
        $this->baseDirectory = $baseDirectoryName;
    }

    public function scanForTasks()
    {
        $tasks = [];
        $directories = Storage::disk('local')->directories($this->baseDirectory);
        foreach ($directories as $dir) {
            $process = new Process('scrapy list', storage_path('app/' . $dir));
            $process->run();

            // executes after the command finishes
            if (!$process->isSuccessful()) {
                \Log::error('the ' . $dir . ' folder is not a valid Scrapy project.');
                continue;
                //throw new ProcessFailedException($process);
            }
            $output = rtrim($process->getOutput());
            $taskNames = explode("\n", $output);
            foreach($taskNames as $task)
              $tasks []= ['directory' => $dir, 'crawler' => $task];
        }
        return $tasks;
    }
}
