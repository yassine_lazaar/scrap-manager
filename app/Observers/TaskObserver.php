<?php

namespace App\Observers;

use App\Models\Task;
use App\Jobs\ProcessTask;

class TaskObserver
{

    /**
     * Listen to the created event.
     *
     * @param  \App\Models\Task  $task
     * @return void
     */
    public function created(Task $task)
    {
        //
    }

    /**
     * Listen to the deleting event.
     *
     * @param  \App\Models\Task  $task
     * @return void
     */
    public function deleting(Task $task)
    {
        //
    }
}
