<?php

namespace App\Models;

use App\Events\SendOutput;
use App\Events\TaskUpdate;
use Symfony\Component\Process\Process;

class Task extends \Jenssegers\Mongodb\Eloquent\Model
{
    protected $fillable = [
        'crawler', 'directory', 'user_id', 'status', 'log_file',
    ];
    protected $dates = [
        'created_at', 'updated_at',
    ];

    protected $dateFormat = 'Y-m-d\TH:i:s\Z';
    protected $hidden = ['user'];

    private $process;

    public function run()
    {
        $logPath = storage_path('logs/crawlers/') . $this->log_file;
        $command = "cd " . storage_path('app/' . $this->directory);
        $command = $command . " && scrapy crawl " . $this->crawler;
        $this->process = new Process($command, storage_path('app'));
        $this->status = TASK_RUNNING_STATUS;
        $this->save();
        event(new TaskUpdate($this));
        $this->process->start();
        $this->process->wait(function ($type, $buffer) use ($logPath) {
            $log = fopen($logPath, 'a');
            if (flock($log, LOCK_EX)) { // do an exclusive lock
                fwrite($log, $buffer);
                flock($log, LOCK_UN); // release the lock
                fclose($log);
            } else {
                \Log::debug("Couldn't get the lock!");
            }
        });
        \File::append($logPath, $this->_id);
        if ($this->process->isSuccessful()) {
            $this->status = TASK_SUCCESS_STATUS;
        } else {
            $this->status = TASK_ERROR_STATUS;
        }
        $this->save();
        event(new TaskUpdate($this));
    }

    public function sendOutput($seek = 0)
    {
        $logPath = storage_path('logs/crawlers/') . $this->log_file;
        try {
            $log = fopen($logPath, 'r');
            if (flock($log, LOCK_EX)) { // do an exclusive lock
                fseek($log, $seek);
                $output = fgets($log);
                $seek += strlen($output);
                $output = trim($output, " \t\n\r\0\x0B");
                if ($output != "") {
                    event(new SendOutput($output, $this->_id));
                }
                flock($log, LOCK_UN); // release the lock
                fclose($log);
                if (strcmp($output, $this->_id) != 0) {
                    return $this->sendOutput($seek);
                }
            } else {
                \Log::debug("Couldn't get the lock!");
            }
        } catch (\Exception $e) {
            \Log::debug($e->getMessage());
        }
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
