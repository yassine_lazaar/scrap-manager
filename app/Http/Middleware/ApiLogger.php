<?php

namespace App\Http\Middleware;

use Closure;

class ApiLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		\Log::info('app.requests', ['request' => $request->url()]);
		\Log::info('app.headers', ['request' => $request->headers]);
		\Log::info('app.req.all', ['request' => $request->all()]);
        return $next($request);
    }

}
