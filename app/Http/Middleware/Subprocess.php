<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\Contracts\TaskRepositoryInterface;

class Subprocess
{

    protected $task;

    public function __construct(TaskRepositoryInterface $task)
    {
        $this->task = $task;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        \Log::debug("Terminating");
        $tsk = $response->getOriginalContent()['data'];
        $tsk->run();
    }
}
