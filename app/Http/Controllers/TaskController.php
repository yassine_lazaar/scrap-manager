<?php

namespace App\Http\Controllers;

use App\Events\TaskCreated;
use App\Repositories\Contracts\TaskRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;
use App\Models\Task;
use App\Jobs\ProcessOutput;

class TaskController extends Controller
{
    protected $task;

    public function __construct(TaskRepositoryInterface $task)
    {
        $this->task = $task;
    }

    public function index(Request $request)
    {
        return response()->success($this->task->all(['user']));
    }

    public function start(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'crawler' => 'required|string',
            'directory' => 'required|string',
        ]);
        if ($validator->fails()) {
            return response()->error($validator->errors());
        }
        $task = $this->task->create([
            'crawler' => $request->get('crawler'),
            'directory' => $request->get('directory'),
            'user_id' => JWTAuth::user()->id,
            'status' => TASK_IDLE_STATUS,
            'log_file' => filter_var($request->get('crawler') . '-' . Carbon::now()->timestamp . '.log', FILTER_SANITIZE_STRING),
        ]);
        event(new TaskCreated($task));
        return response()->success($task);
    }

    public function output(Request $request)
    {
        $validator = Validator::make($request->all(), [
            '_id' => 'required|exists:tasks',
        ]);
        if ($validator->fails()) {
            return response()->error($validator->errors());
        }
        $task = $this->task->getById($request->get('_id'));
        ProcessOutput::dispatch($task)->onQueue('output-queue');
        return response()->success($task);
    }
}
