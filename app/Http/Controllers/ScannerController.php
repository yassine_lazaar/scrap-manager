<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scanner\TaskScanner as TaskScanner;

class ScannerController extends Controller
{

    private $scanner;

    public function __construct(){
        $this->scanner = app('TaskScanner');
    }

    public function index()
    {
        $availableTasks = $this->scanner->scanForTasks();
        return response()->success($availableTasks);
    }
}
