<?php

namespace App\Http\Controllers\Traits;

use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Facades\JWTAuth;

trait TokenResponseFormat
{
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->success([
            'accessToken' => $token,
            'tokenType' => 'bearer',
            'expiresIn' => JWTFactory::getTTL() * 60,
            'user' => JWTAuth::user()->id
        ]);
    }
}