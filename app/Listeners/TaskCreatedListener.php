<?php

namespace App\Listeners;

use App\Events\TaskCreated;
use App\Jobs\ProcessTask;

class TaskCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  App\Events\TaskCreated $event
     * @return void
     */
    public function handle(TaskCreated $event)
    {
        $logPath = storage_path('logs/crawlers/') . $event->task->log_file;
        \File::put($logPath, '');
        //shell_exec('sudo chown www-data:www-data '. $logPath);
        ProcessTask::dispatch($event->task)->onQueue('default');
    }
}
