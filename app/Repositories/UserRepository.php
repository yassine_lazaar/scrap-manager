<?php 

namespace App\Repositories;

use App\Repositories\Contracts\UserRepositoryInterface;

class UserRepository extends AbstractEloquentRepository implements UserRepositoryInterface
{
    /**
     * Create new user
     * 
     * @param array $data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function create(array $data) {
        return $this->model->create($data);
    }

    /**
     * Update user data
     * 
     * @param array $data
     * @param $id
     * @return int
     */
    public function update(array $data, $id) {
        return $this->model->where('id', '=', $id)->update($data);
    }

    /**
     * Delete user
     * 
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        return $this->model->destroy($id);
    }
}