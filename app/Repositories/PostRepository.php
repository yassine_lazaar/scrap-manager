<?php

namespace App\Repositories;

use App\Repositories\Contracts\PostRepositoryInterface;

class PostRepository extends AbstractEloquentRepository implements PostRepositoryInterface
{
    /**
     * Create new post
     * 
     * @param array $data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function create(array $data) {
        return $this->model->create($data);
    }

    /**
     * Update post data
     * 
     * @param array $data
     * @param $id
     * @return int
     */
    public function update(array $data, $id) {
        return $this->model->where('id', '=', $id)->update($data);
    }

    /**
     * Delete post
     * 
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        return $this->model->destroy($id);
    }
}