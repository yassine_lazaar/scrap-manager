<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractEloquentRepository
{
    protected $model;

    /**
     * Constructor
     * 
     * @param Illuminate\Database\Eloquent\Model model
     * @return void
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $with
     * @return Illuminate/Database/Eloquent/Builder
     */
    public function make(array $with = array())
    {
        return $this->model->with($with);
    }

    /**
     * Return all entities
     *
     * @param array $columns
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all(array $with = array(), $columns = array('*'))
    {
        return $this->make($with)->get($columns);
    }

    /**
     * Find an entity by id
     *
     * @param int $id
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id, array $with = array(), $columns = array('*'))
    {
        return $this->make($with)->find($id, $columns);
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getFirstBy($key, $value, array $with = array(), $columns = array('*'))
    {
        return $this->make($with)->where($key, '=', $value)->first($columns);
    }

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getManyBy($key, $value, array $with = array(), $columns = array('*'))
    {
        return $this->make($with)->where($key, '=', $value)->get($columns);
    }

    /**
     * Find many entities by multiple filters
     *
     * @param array $filters
     * @param array $with
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getManyWhere($filters, array $with = array(), $columns = array('*'))
    {
        return $this->make($with)->where($filters)->get($columns);
    }

    /**
     * Get results by page
     *
     * @param int $perPage
     * @param array $with
     * @return Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 1, $with = array(), $columns = array('*'))
    {
        return $this->model->make($with)->paginate($perPage, $columns);
    }

    /**
     * Return all results that have a required relationship
     *
     * @param string $relation
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function has($relation, array $with = array(), $columns = array('*'))
    {
        $entity = $this->make($with);
        return $entity->has($relation)->get($columns);
    }

    /**
     * Get model count
     *
     * @return integer
     */
    public function count()
    {
        return $this->model->count();
    }
}
