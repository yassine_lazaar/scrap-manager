<?php

namespace App\Repositories;

use App\Repositories\Contracts\TaskRepositoryInterface;

class TaskRepository extends AbstractEloquentRepository implements TaskRepositoryInterface
{
    /**
     * Create new task
     *
     * @param array $data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update task data
     *
     * @param array $data
     * @param $id
     * @return int
     */
    public function update(array $data, $id)
    {
        return $this->model->where('id', '=', $id)->update($data);
    }

    /**
     * Delete task
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }
}
