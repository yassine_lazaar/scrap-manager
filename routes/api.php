<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::post('/register', 'AuthController@register');
    Route::post('/login', 'AuthController@login');
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'auth'], function () {
    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');
    Route::get('/me', 'AuthController@me');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/scan', 'ScannerController@index');
    Route::get('/task', 'TaskController@index');
    Route::post('/task/output', 'TaskController@output');
    Route::post('/task/start', 'TaskController@start');
});

