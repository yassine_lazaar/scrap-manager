<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channelsp
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
 */

Broadcast::channel('Task.{id}', function ($user, $id) {
    //\Log::debug(json_encode($user));
    return true;
});

Broadcast::channel('Tasks.{id}', function ($user, $id) {
    return $user->id == $id;
});
