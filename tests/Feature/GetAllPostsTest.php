<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetAllPostsTest extends TestCase
{
    public function testGetAllPosts()
    {
        $post = factory(\App\Models\Post::class)->create();

        $this->get('api/posts')
        ->assertJsonFragment([
            'id' => $post->id,
            'title' => $post->title,
        ]);
    }

    function setUp()
    {
        parent::setUp();
        config(['app.url' => 'http://scrapmanager.test']);
    }
}
